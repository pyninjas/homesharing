from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from reversion.admin import VersionAdmin
from user.models import User


@admin.register(User)
class UserAdmin(VersionAdmin, UserAdmin):
    # list_display = ()
    # list_filter = ()
    # fieldsets = ()
    readonly_fields = ('created_at',)
    # search_fields = ('user__first_name', 'user__last_name', 'note')
    # autocomplete_fields = ()
