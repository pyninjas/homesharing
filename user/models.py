from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils import timezone
from decimal import Decimal


class User(AbstractUser):
    """Custom User Model."""
    created_at = models.DateTimeField(_("Added"), auto_now_add=True)

    @property
    def total_invoices(self) -> Decimal:
        now = timezone.now()
        total = Decimal('0.0')
        for invoice in self.invoices.filter(is_paid=True, date__year=now.year, date__month=now.month):
            total += invoice.amount
        return total

    @property
    def total_shoppings(self) -> Decimal:
        now = timezone.now()
        total = Decimal('0.0')
        for shopping in self.shoppings.filter(is_purchased=True, date__year=now.year, date__month=now.month):
            total += shopping.amount
        return total

    @property
    def total_spent(self) -> Decimal:
        return self.total_invoices + self.total_shoppings

    @property
    def earnings(self) -> Decimal:
        from invoice.models import Invoice  # NOQA
        from shopping.models import Shopping  # NOQA
        now = timezone.now()
        total = Decimal('0.0')
        for invoice in Invoice.objects.filter(is_paid=True, date__year=now.year, date__month=now.month):
            total += invoice.amount
        for shopping in Shopping.objects.filter(is_purchased=True, date__year=now.year, date__month=now.month):
            total += shopping.amount
        total_users = User.objects.filter(is_active=True).count()
        return self.total_spent - (total / total_users)

    @property
    def full_name(self) -> str:
        return ' '.join([str(self.first_name), str(self.last_name)])

    def __str__(self) -> str:
        if self.full_name:
            return self.full_name
        return self.username

    class Meta:
        verbose_name = _("User")
        verbose_name_plural = _("Users")
        ordering = ('first_name', 'last_name',)
