from homesharing.base import INSTALLED_APPS, MIDDLEWARE  # NOQA
# import raven
# import os

SECRET_KEY = '...'
DEBUG = False
DEBUG_TOOLBAR = False

TIME_ZONE = 'Europe/Istanbul'

# Email Settings
EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_HOST = ""
EMAIL_PORT = 465
EMAIL_HOST_USER = ""
EMAIL_HOST_PASSWORD = ""
EMAIL_USE_SSL = True
# EMAIL_USE_TLS = False
DEFAULT_FROM_EMAIL = ""
DEFAULT_CONTACT_EMAIL = DEFAULT_FROM_EMAIL

ALLOWED_HOSTS = ["*"]

# Sentry
# INSTALLED_APPS.append('raven.contrib.django.raven_compat')
# RAVEN_CONFIG = {
#     'dsn': '',
#     'release': raven.fetch_git_sha(os.path.dirname(os.pardir)),
# }
