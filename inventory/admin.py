from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from reversion.admin import VersionAdmin
from inventory.models import Inventory


@admin.register(Inventory)
class InventoryAdmin(VersionAdmin):
    list_display = ('pk', 'name', 'owner', 'price', 'quantity', 'is_secondhand', 'is_sold', 'date',)
    list_filter = ('is_sold', 'is_secondhand', 'date',)
    fieldsets = (
        (_('Inventory Item Details'), {
            'fields': (
                'name', 'owner', ('price', 'quantity'), 'is_secondhand', 'source', 'note', ('is_sold', 'sale_price'),
                ('date', 'created_at')
            )
        }),
    )
    readonly_fields = ('created_at',)
    search_fields = ('name', 'owner__first_name', 'owner__last_name', 'source', 'note')
    autocomplete_fields = ('owner',)
