from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.contrib.auth import get_user_model
from django.core.validators import MinValueValidator
from decimal import Decimal


class Inventory(models.Model):
    name = models.CharField(_("Name"), max_length=150)
    owner = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, verbose_name=_("Owner"),
        related_name='items'
    )
    price = models.DecimalField(
        _("Price"), default=0, max_digits=6, decimal_places=2, validators=[MinValueValidator(Decimal('0.00'))]
    )
    quantity = models.IntegerField(_("Quantity"), default=1)
    is_secondhand = models.BooleanField(_("Second hand"), default=False)
    source = models.TextField(_("Source of the purchase"), null=True, blank=True)
    is_sold = models.BooleanField(_("Sold"), default=False)
    sale_price = models.DecimalField(
        _("Sale price"), max_digits=6, decimal_places=2, null=True, blank=True
    )
    note = models.TextField(_("Note"), null=True, blank=True)
    date = models.DateField(_("Purchase Date"), null=True, blank=True)
    created_at = models.DateTimeField(_("Record Added"), auto_now_add=True)

    class Meta:
        verbose_name = _("Inventory")
        verbose_name_plural = _("Inventories")
        ordering = ('-date', '-created_at')
