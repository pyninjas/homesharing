# homesharing

System to track payments in shared apartments.

INSTALL
-------
* Clone and setup environment, install requirements: `pip install -r requirements.txt`
* Migrate database: `./manage.py migrate`
* Run test server: `./manage.py runserver`
* Check `bin` folder for production setup examples
* Add cron work to delete unused history: `./manage.py deleterevisions --days=90`
