from django.utils.translation import ugettext_lazy as _
from django.shortcuts import render
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.utils import timezone
from django.http import JsonResponse
from django.db.models import Sum
from decimal import Decimal
from user.models import User
from invoice.models import Invoice
from shopping.models import Shopping


@login_required(redirect_field_name='next')
def index(request):
    now = timezone.now()
    data = {
        'users': get_user_model().objects.filter(is_active=True),
        'currency_class': settings.CURRENCY_CLASS,
        'current_month': now.month,
        'current_year': now.year
    }
    return render(request, 'web/index.html', data)


@login_required(redirect_field_name='next')
def spendings_data(request) -> JsonResponse:
    now = timezone.now()
    try:
        month = int(request.POST.get('month', now.month))
        year = int(request.POST.get('year', now.year))
    except ValueError:
        return JsonResponse({'error': _("Month and Year should be integers")}, status=400)
    data = {'users': []}
    users = User.objects.filter(is_active=True)
    # Total invoice spendings for all users
    total_invoice_spendings = Invoice.objects.filter(
        is_paid=True, date__year=year, date__month=month
    ).aggregate(Sum('amount'))['amount__sum']
    if not total_invoice_spendings:
        total_invoice_spendings = Decimal('0')
    # Total shopping spendings for all users
    total_shopping_spendings = Shopping.objects.filter(
        is_purchased=True, date__year=year, date__month=month
    ).aggregate(Sum('amount'))['amount__sum']
    if not total_shopping_spendings:
        total_shopping_spendings = Decimal('0')
    for user in users:
        # Total invoice spendings for the current user
        total_invoices = user.invoices.filter(
            is_paid=True, date__year=year, date__month=month
        ).aggregate(Sum('amount'))['amount__sum']
        if not total_invoices:
            total_invoices = Decimal('0')
        # Total shopping spendings for the current user
        total_shoppings = user.shoppings.filter(
            is_purchased=True, date__year=year, date__month=month
        ).aggregate(Sum('amount'))['amount__sum']
        if not total_shoppings:
            total_shoppings = Decimal('0')
        total_user_spendings = total_invoices + total_shoppings

        data['users'].append({
            'user': user.username,
            'total_invoices': round(total_invoices, 2),
            'total_shoppings': round(total_shoppings, 2),
            'total_spent': round(total_user_spendings, 2),
            'earnings': round(total_user_spendings - (total_invoice_spendings + total_shopping_spendings) / users.count(), 2)
        })
    return JsonResponse(data, status=200)
