function update_spendings() {
  var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
  var chosen_month = $('.spendings_month_select').val();
  var chosen_year = $('.spendings_year_select').val();
  $.ajax({
    url: $('.spendings_month_select').data('endpoint'),
    type: "POST",
    dataType: "json",
    data: {
      "month": chosen_month,
      "year": chosen_year
    },
    cache: false,
    beforeSend: function(xhr, settings) {
      xhr.setRequestHeader("X-CSRFToken", csrftoken);
    },
    success: function(data) {
      $.each(data.users, function(index, item) {
        var userbox = $('.details-' + item.user);
        var earnings_box = userbox.find('.balance-details');
        userbox.find('.user-total-spent').text(item.total_spent);
        earnings_box.find('.user-total-earnings').text(item.earnings);
        earnings_box.removeClass('balance-red');
        earnings_box.removeClass('balance-green');
        if (item.earnings < 0) {
          earnings_box.addClass('balance-red');
        } else {
          earnings_box.addClass('balance-green');
        };
      });
    },
    error: function(data) {
      var response = $.parseJSON(data.responseText).error;
    },
    complete: function() {}
  })
};

$(document).ready(
  $('.spendings_month_select').on('change', function(event) {
    event.preventDefault();
    update_spendings();
  })
);
$(document).ready(
  $('.spendings_year_select').on('change', function(event) {
    event.preventDefault();
    update_spendings();
  })
);
