from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from django.conf import settings as django_settings
from django.conf.urls.static import static
from django.conf.urls.i18n import i18n_patterns
from django.urls import path, include
from django.views.generic.base import RedirectView
from web import views as webclient


urlpatterns = [
    path('i18n/', include('django.conf.urls.i18n')),
]

urlpatterns += i18n_patterns(
    path('admin/', admin.site.urls),
    path('favicon.ico', RedirectView.as_view(url='/static/favicon.ico', permanent=True)),
    path('robots.txt', RedirectView.as_view(url='/static/robots.txt', permanent=True)),
    path('spendings', webclient.spendings_data, name='spendings'),
    path('', webclient.index, name='index')
)

admin.site.site_title = _('HomeSharing Admin')
admin.site.site_header = _('HomeSharing')
admin.site.index_title = _('Home administration')

if django_settings.DEBUG:
    urlpatterns += static(django_settings.MEDIA_URL, document_root=django_settings.MEDIA_ROOT)
    urlpatterns += static(django_settings.STATIC_URL, document_root=django_settings.STATIC_ROOT)
