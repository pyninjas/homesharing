# Generated by Django 2.1 on 2019-08-01 21:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('invoice', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invoice',
            name='invoice_type',
            field=models.IntegerField(choices=[(1, 'Others'), (2, 'Electricity'), (3, 'Water'), (4, 'Natural Gas'), (5, 'Internet'), (6, 'Apartment Fee'), (7, 'Drinking Water')], default=1, verbose_name='Type'),
        ),
    ]
