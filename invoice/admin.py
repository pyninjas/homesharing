from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from reversion.admin import VersionAdmin
from invoice.models import Invoice, DrinkingWater


@admin.register(Invoice)
class InvoiceAdmin(VersionAdmin):
    list_display = ('pk', 'invoice_type', 'user', 'amount', 'is_paid', 'date',)
    list_filter = ('invoice_type', 'date', 'user', 'is_paid',)
    fieldsets = (
        (_('Invoice Details'), {
            'fields': (
                ('invoice_type', 'amount'), 'user', 'date', 'note', ('is_paid', 'created_at')
            )
        }),
    )
    readonly_fields = ('created_at',)
    search_fields = ('user__first_name', 'user__last_name', 'note')
    autocomplete_fields = ('user',)


@admin.register(DrinkingWater)
class DrinkingWaterAdmin(VersionAdmin):
    list_display = ('pk', 'quantity', 'is_paid', 'date',)
    list_filter = ('is_paid', 'date',)
    fieldsets = (
        (_('Invoice Details'), {
            'fields': (
                ('quantity', 'date'), ('is_paid', 'created_at')
            )
        }),
    )
    readonly_fields = ('created_at',)
    # search_fields = ()
    # autocomplete_fields = ()
