from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.contrib.auth import get_user_model
from django.core.validators import MinValueValidator
from django.utils import timezone
from decimal import Decimal


class Invoice(models.Model):
    INVOICE_OTHER = 1
    INVOICE_ELECTRICITY = 2
    INVOICE_WATER = 3
    INVOICE_NATURAL_GAS = 4
    INVOICE_INTERNET = 5
    INVOICE_APARTMENT_FEE = 6
    INVOICE_DRINKING_WATER = 7
    InvoiceTypes = (
        (INVOICE_OTHER, _('Others')),
        (INVOICE_ELECTRICITY, _('Electricity')),
        (INVOICE_WATER, _('Water')),
        (INVOICE_NATURAL_GAS, _('Natural Gas')),
        (INVOICE_INTERNET, _('Internet')),
        (INVOICE_APARTMENT_FEE, _('Apartment Fee')),
        (INVOICE_DRINKING_WATER, _('Drinking Water')),
    )
    invoice_type = models.IntegerField(_("Type"), choices=InvoiceTypes, default=1)
    user = models.ForeignKey(
        get_user_model(), on_delete=models.SET_NULL, verbose_name=_("Payer"),
        related_name='invoices', null=True, blank=True
    )
    amount = models.DecimalField(
        _("Amount"), default=0, max_digits=6, decimal_places=2, validators=[MinValueValidator(Decimal('0.00'))]
    )
    note = models.TextField(_("Note"), null=True, blank=True)
    # @TODO photo
    is_paid = models.BooleanField(_("Paid"), default=True)
    date = models.DateField(_("Payment Date"), null=True, blank=True)
    created_at = models.DateTimeField(_("Record Added"), auto_now_add=True)

    class Meta:
        verbose_name = _("Invoice")
        verbose_name_plural = _("Invoices")
        ordering = ('date', '-created_at')


class DrinkingWater(models.Model):
    quantity = models.IntegerField(_("Quantity"), default=1, validators=[MinValueValidator(Decimal('0.00'))])
    is_paid = models.BooleanField(_("Paid"), default=False)
    date = models.DateTimeField(_("Date"), default=timezone.now)
    created_at = models.DateTimeField(_("Record Added"), auto_now_add=True)

    class Meta:
        verbose_name = _("Drinking Water")
        verbose_name_plural = _("Drinking Water")
        ordering = ('date', '-created_at')
