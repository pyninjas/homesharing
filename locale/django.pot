# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-08-09 11:18+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: homesharing/settings.py:85
msgid "English"
msgstr ""

#: homesharing/settings.py:86
msgid "Turkish"
msgstr ""

#: homesharing/urls.py:16
msgid "HomeSharing Admin"
msgstr ""

#: homesharing/urls.py:17 web/templates/web/base.html:9
#: web/templates/web/base.html:41
msgid "HomeSharing"
msgstr ""

#: homesharing/urls.py:18
msgid "Home administration"
msgstr ""

#: invoice/admin.py:12 invoice/admin.py:28
msgid "Invoice Details"
msgstr ""

#: invoice/models.py:18
msgid "Others"
msgstr ""

#: invoice/models.py:19
msgid "Electricity"
msgstr ""

#: invoice/models.py:20
msgid "Water"
msgstr ""

#: invoice/models.py:21
msgid "Natural Gas"
msgstr ""

#: invoice/models.py:22
msgid "Internet"
msgstr ""

#: invoice/models.py:23
msgid "Appartment Fee"
msgstr ""

#: invoice/models.py:24 invoice/models.py:53 invoice/models.py:54
msgid "Drinking Water"
msgstr ""

#: invoice/models.py:26
msgid "Type"
msgstr ""

#: invoice/models.py:28
msgid "Payer"
msgstr ""

#: invoice/models.py:32 shopping/models.py:15
msgid "Amount"
msgstr ""

#: invoice/models.py:34 shopping/models.py:19
msgid "Note"
msgstr ""

#: invoice/models.py:36 invoice/models.py:48
msgid "Paid"
msgstr ""

#: invoice/models.py:37
msgid "Payment Date"
msgstr ""

#: invoice/models.py:38 invoice/models.py:50 shopping/models.py:21
msgid "Record Added"
msgstr ""

#: invoice/models.py:41
msgid "Invoice"
msgstr ""

#: invoice/models.py:42 web/templates/web/base.html:49
msgid "Invoices"
msgstr ""

#: invoice/models.py:47
msgid "Quantity"
msgstr ""

#: invoice/models.py:49 shopping/models.py:20
msgid "Date"
msgstr ""

#: shopping/admin.py:12
msgid "Shopping Details"
msgstr ""

#: shopping/models.py:9
msgid "Name"
msgstr ""

#: shopping/models.py:11
msgid "Buyer"
msgstr ""

#: shopping/models.py:17
msgid "Purchased"
msgstr ""

#: shopping/models.py:18
msgid "Shared"
msgstr ""

#: shopping/models.py:24
msgid "Shopping"
msgstr ""

#: shopping/models.py:25
msgid "Shoppings"
msgstr ""

#: user/models.py:10
msgid "Added"
msgstr ""

#: user/models.py:55
msgid "User"
msgstr ""

#: user/models.py:56
msgid "Users"
msgstr ""

#: web/templates/web/base.html:7
msgid "Share invoices with home mates and shopping list"
msgstr ""

#: web/templates/web/base.html:43
msgid "Menu"
msgstr ""

#: web/templates/web/base.html:52
msgid "Shopping List"
msgstr ""

#: web/templates/web/index.html:10
msgid "Avatar"
msgstr ""
