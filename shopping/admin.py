from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from reversion.admin import VersionAdmin
from shopping.models import Shopping


@admin.register(Shopping)
class ShoppingAdmin(VersionAdmin):
    list_display = ('pk', 'name', 'user', 'amount', 'is_purchased', 'date',)
    list_filter = ('date', 'user', 'is_purchased',)
    fieldsets = (
        (_('Shopping Details'), {
            'fields': (
                ('name'), ('amount', 'user'), 'date', 'note', ('is_purchased', 'created_at')
            )
        }),
    )
    readonly_fields = ('created_at',)
    search_fields = ('name', 'user__first_name', 'user__last_name', 'note')
    autocomplete_fields = ('user',)
