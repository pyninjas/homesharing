from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.contrib.auth import get_user_model
from django.core.validators import MinValueValidator
from decimal import Decimal


class Shopping(models.Model):
    name = models.CharField(_("Name"), max_length=255)
    user = models.ForeignKey(
        get_user_model(), on_delete=models.SET_NULL, verbose_name=_("Buyer"),
        related_name='shoppings', null=True, blank=True
    )
    amount = models.DecimalField(
        _("Amount"), default=0, max_digits=6, decimal_places=2, validators=[MinValueValidator(Decimal('0.00'))]
    )
    is_purchased = models.BooleanField(_("Purchased"), default=True)
    is_shared = models.BooleanField(_("Shared"), default=True)
    note = models.TextField(_("Note"), null=True, blank=True)
    date = models.DateField(_("Date"), null=True, blank=True)
    created_at = models.DateTimeField(_("Record Added"), auto_now_add=True)

    class Meta:
        verbose_name = _("Shopping")
        verbose_name_plural = _("Shoppings")
        ordering = ('date', '-created_at')
